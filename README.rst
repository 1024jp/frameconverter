========================
FrameConverter
========================

CSVファイル内の動画フレームデータを一括で秒に書き換えるコマンドライン・スクリプトです。
modules/frameconverter.py単体でPythonのモジュールとしても使えます。


******************
環境要件
******************

Python 2.x or 3.x


******************
使い方
******************

Terminalから任意の引数を付けて実行します。

取りうる引数について詳しくは、::

    $ ./frameconverter.py --help

で表示されるヘルプをご覧下さい。

例1
============
::

    $ ./frameconverter.py source.csv --out result.csv --delimiter tab --frame_col 2 --time_col 3 --start_frame 100

この場合、source.csv を TSV (tab-separated values) ファイルとして解釈し、ファイル内の2列目にあるframeデータをstart_frame分ずらして秒に直したものファイル3列目にセットし result.csv に書き出すことになります。


******************
クレジット
******************
Author: 1024jp