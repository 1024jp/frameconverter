#!/usr/bin/env python

"""Commandline interface of frameconverter.py.

Convert poster frame to second in a csv file.
"""

import csv

from modules import argsparser, frameconverter


__date__ = '2016-01-19'
__author__ = '1024jp <http://wolfrosch.com/>'


def convert_csv(converter, input_file, output_file, delimiter=',',
                frame_col=1, time_col=2):
    """Convert poster frame in csv file and write them in a new file.

    arguments:
    converter -- FrameConverter instance.
    input_file -- file object of source file with a read permission.
    output_file -- file object to write result data with a write permission.
    delimiter -- delimiter of csv file.
    frame_col -- column number of frame in the csv file. (number starts with 1)
    time_col -- column number of time in the csv file. (number starts with 1)
    """
    frame_index = frame_col - 1
    time_index = time_col - 1

    with input_file, output_file:
        reader = csv.reader(input_file, delimiter=delimiter)
        writer = csv.writer(output_file, delimiter=delimiter,
                            lineterminator='\n')

        # check header
        has_header = csv.Sniffer().has_header(input_file.readline())
        input_file.seek(0)
        if has_header:
            header = next(reader)
            writer.writerow(header)

        for row in reader:
            frame = int(row[frame_index])

            # transform
            time = converter.convert(frame)

            # rewrite row
            row[time_index] = time

            # write
            writer.writerow(row)


if __name__ == "__main__":
    # parse command-line options
    args = argsparser.parse()

    # create converter instance
    converter = frameconverter.FrameConverter(args.start_frame)

    # execute
    convert_csv(converter, args.file, args.out, delimiter=args.delimiter,
                frame_col=args.frame_col, time_col=args.time_col)
