#!/usr/bin/env python

"""Command line arguments parser.
"""

import argparse
import logging
import os

try:
    from . import __version__ as version
except:
    version = 'n/a'


class DelimiterAction(argparse.Action):
    """Return delimiter symbol."""
    DELIMITERS = {
        'comma': ',',
        'tab': '\t',
        'space': ' ',
    }

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, self.DELIMITERS[values])


def parse():
    """Parse command line arguments."""

    # create parser instance
    parser = argparse.ArgumentParser(description='Convert frame '
                                                 'in csv file.')

    # argument
    parser.add_argument('file',
                        type=argparse.FileType('rU'),
                        metavar='FILE',
                        help="path to source file"
                        )

    # define options
    parser.add_argument('--version',
                        action='version',
                        version=version
                        )

    # output file
    outfile = parser.add_argument_group(
        'out file options',
        'In default, the out file will be created in the same directory of '
        'source file with the prefix.')
    outfile.add_argument('--out',
                         type=argparse.FileType('w'),
                         default=None,
                         metavar='FILE',
                         help="set path to output file"
                         )
    outfile.add_argument('--prefix',
                         type=str,
                         default='new',
                         help=("set prefix of out file name "
                               "(default: '%(default)s')")
                         )

    # file format
    fileformat = parser.add_argument_group('file format options')
    fileformat.add_argument('--delimiter',
                            type=str,
                            choices=DelimiterAction.DELIMITERS,
                            action=DelimiterAction,
                            default=',',
                            help="set delimiter of file (default: comma)"
                            )
    fileformat.add_argument('--frame_col',
                            type=int,
                            default=1,
                            metavar='INDEX',
                            help=("set column positions of frame in file "
                                  "(default: %(default)s)")
                            )
    fileformat.add_argument('--time_col',
                            type=int,
                            default=2,
                            metavar='INDEX',
                            help=("set column positions of time in file "
                                  "(default: %(default)s)")
                            )

    # transform values
    transform = parser.add_argument_group('transform options')
    transform.add_argument('--start_frame',
                           type=int,
                           default=0,
                           metavar='FRAME',
                           help="set start frame to cut off"
                           )
    transform.add_argument('--fps',
                           type=float,
                           default=30,
                           help=("set frame per second "
                                 "(default: %(default)s)")
                           )

    output = parser.add_argument_group('output options')
    output.add_argument('-v', '--verbose',
                        action='store_true',
                        default=False,
                        help="display debug info to standard output"
                        )

    # parse arguments
    args = parser.parse_args()

    # set outfile path
    if not args.out:
        dirpath = os.path.dirname(args.file.name)
        filename = os.path.basename(args.file.name)
        out_path = os.path.join(dirpath, args.prefix + '_' + filename)
        args.out = file(out_path, 'w')

    # set logging level
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG,
                            format='[%(levelname)s] %(module)s -'
                                   '%(message)s (%(relativeCreated)4dms)')
        display(args)

    return args


def display(args):
    """Display inputted arguments for test use."""

    print('[arguments]')
    for key, arg in vars(args).iteritems():
        if isinstance(arg, file):
            arg = arg.name
        print('    {:10s} {}'.format(key + ':', arg))


if __name__ == "__main__":
    display(parse())
