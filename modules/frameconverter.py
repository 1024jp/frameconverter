#!/usr/bin/env python

"""Rotate and move inputted coordinates.
"""

__date__ = '2016-01-19'
__author__ = '1024jp <http://wolfrosch.com/>'

class FrameConverter:
    fps = 30

    def __init__(self, start_frame=0, fps=None):
        self.start_frame = start_frame
        if fps is not None:
            self.fps = fps

    def convert(self, frame):
        return self._convert_frame_to_second(frame - self.start_frame)

    def _convert_frame_to_second(self, frame):
        """Convert frame index to time second.

        Arguments:
        frame (int)

        Return:
        second (float)
        """
        return float(frame) / self.fps
